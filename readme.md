# Noiz Backend #

The most simplest [UDP](http://en.wikipedia.org/wiki/User_Datagram_Protocol) client and server written in Python 
with usage of standard python [socket library](https://docs.python.org/2/library/socket.html). 

## What those scripts are for?
Basically. just for demonstration of the ability to transfer
files to server using UDP and saving them in destination folder.

* __noiz-backend/server.py__ script starting a server
* __noiz-backend/send.py__ sending file to server
 
## Usage and examples

### Linux
Just [install](http://docs.python-guide.org/en/latest/starting/install/linux/) Python interpreter on you machine and after cloning repository change directory to project root. 

__Run server__ by running a command:
```
user@pc$ python server.py -s <ip:port>
```
For example:
```
user@pc$ python server.py -s 127.0.0.1:4343
UDP server listening at 127.0.0.1:4243
```
You should see same output if you will launch it with those arguments. The sever address
 is optional, by default it will listen to 127.0.0.1:4000

__For sending file__ file to server you have to run:
```
user@pc$ python send.py  <path_to_file> [-s] <server_address>
```
Server address (-s) is also optional and by default 127.0.0.1:4000 will be used. 
For example, after launching like this:
```
user@pc$ python send.py ../../pic.jpg
```
script will send specified file to server by address 127.0.0.1:4000 and on your server's output you should see:
```
Data from ip 127.0.0.1 flushed to file ./tmp/<current_timestamp>_127.0.0.1.pic.jpg.
```

### How to send data from custom enviroment? ###

The file sending algorithm is ever possibly easiest. __It is not reliable at all__ and created just for testing purposes.
Server is listening for 1024 bytes packets. In any of each of them every first byte representing service information and __must have__
integer value, not greater the 3, depended on which the rest of bytes in packet will be interpreted as:

* file name (if first byte is) -- 1
* file content -- 2
* end of file content -- 3

After the last listed packet type server will flush sent data to file in specified folder (default is "noiz-backend/tmp") with
a name created by concatenating bytes from packets with __1__ as first byte.

For example, if client would send packets like:
```
1message
1.txt
2abc
2def
2ghi
3
```
Server will flush data to file:
```
noiz-backend/tmp/<received_timestamp>_<remote_ip>_message.txt
```
and it's content will be "abcdefghi".
