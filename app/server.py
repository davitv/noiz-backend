import socket
import os
import datetime
from connection import Connection, ConnectionsContainer


socket_instance = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

connectionsContainer = ConnectionsContainer()

def start_server(ip, port, output_folder):
    
    if not os.path.isdir(output_folder):
        os.mkdir(output_folder)

    socket_instance.bind((ip, port,))
    
    print 'UDP server listening at %s:%s' % (ip, port)
    
    while True:
      data, (client_host, client_port,) = socket_instance.recvfrom(1024) # buffer size is 1024 bytes
      # print "Connection attempt from %s:%s" % (client_host, client_port,)
      connectionsContainer.fetch_recieved_data(ip, data, output_folder)
      
