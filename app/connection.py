import datetime
import os

class Connection(object):
    
    DATA = ''
   

    def __init__(self, remote_ip, output_folder):
        self.remote_ip = remote_ip
        self.output_folder = output_folder
        self.filetype = ''
        self.full_path = ''

    @property
    def data(self):
        return self.DATA
    
    @data.setter
    def data(self, value):
        if value[0] == '1':
            self.filetype += value[1:]
        elif value[0] == '2':
            self.DATA += value[1:]
        elif value[0] == '3':
            self.flush_data()
            self.filetype = ''
            self.DATA = ''
            print 'Data from ip %s flushed to file %s.' % (self.remote_ip, self.full_path)

    def flush_data(self):
        now = datetime.datetime.now().strftime('%d-%m_%H-%M-%S-%I')
        args = (now, self.remote_ip, self.filetype,) 
        self.full_path = os.path.join(self.output_folder, '%s_%s.%s' % args)
        f = file(self.full_path, 'a')
        f.write(self.data)
        f.close()

class ConnectionsContainer(object):
    
    def __init__(self):
        self.connections_dict = dict()
    
    def get_connection(self, remote_ip, output_folder):
        if remote_ip not in self.connections_dict:
            self.connections_dict[remote_ip] = Connection(remote_ip, output_folder)
    
        connection = self.connections_dict[remote_ip]
    
        return connection

    def fetch_recieved_data(self, remote_ip, data, output_folder):
        connection = self.get_connection(remote_ip, output_folder)
        connection.data = data
        