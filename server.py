import argparse
from app.server import start_server



parser = argparse.ArgumentParser()
parser.add_argument("-o", "--output_folder", help='absolute path to folder, where received files are stored')
parser.add_argument("-s", "--server_address", help='changing the server address')

DEFAULTS = dict(
    server_address='127.0.0.1:4000',
    output_folder='./tmp'
)

def run():
    options = parser.parse_args()
    output_folder = options.output_folder or DEFAULTS['output_folder']

    server_address = options.server_address or DEFAULTS['server_address']
    tmp = server_address.split(':')
    ip = tmp[0]
    port = int(tmp[1])

    start_server(ip, port, output_folder)


if __name__ == '__main__':
    __package__ = __package__ or 'noiz-backend'
    run()