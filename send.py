import argparse
from app.exceptions import *
import os
import socket

DEFAULTS = {
    'server_address': '127.0.0.1:4000'
}

socket_instance = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

def send_to_server(content, server_address):
    """
    Sending content to server by server_address.  
    :param content: bytes
    :param server_address: tuple(ip<str>, port<int>,)
    :return: void
    """
    socket_instance.sendto(content, server_address)

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

parser = argparse.ArgumentParser()
parser.add_argument("file_path", help='absolute path to file which will be sent to server')
parser.add_argument("-s", "--server_address", help='optional argument, changing the server address')


def send_file(abs_file_path, server_address):
    """
    Splitting file to 1023 bytes chunks,
    adding first signal byte and sending
    data to speified server_address.  
    :param abs_file_path: str, absolute path to file
    :param server_address: tuple(ip, port,)
    :return: void
    """
    send_to_server('1' + os.path.basename(abs_file_path), server_address)
    f = open(abs_file_path, 'rb')
    while True:
        data = f.read(1023)
        if not data:
            break
        data = '2' + data
        send_to_server(data, server_address)
    send_to_server('3', server_address)
    

def run():
    options = parser.parse_args()
    if options.server_address is not None:
        settings = options.server_address
    else:
        settings = DEFAULTS['server_address']
    tmp = settings.split(':')
    server_address = (tmp[0], int(tmp[1]),)
    send_file(options.file_path, server_address)
            


if __name__ == '__main__':
    __package__ = __package__ or 'noiz-backend'
    run()